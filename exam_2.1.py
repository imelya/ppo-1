#!/usr/bin/env python3
import ser
from db import db
import sys
from time import sleep
from config import config

hello_string = '\n===========\nДля вывода последовательнсти цифр на МСИ введите S с клавиатуры и нажмите Enter\n\
Для выхода нажмите b и затем Enter\n\n'

stop_commands = config.get('stop_commands')
start_commands = config.get('start_commands')
delay = config.get('delay')

def clear():
    with open('log', 'w') as fd:
        fd.write('')

while 1:
    try:
        command = input(hello_string)
        if len(command) != 1:
            print('Введена неверная команда. Повторите ввод.')
        elif command in stop_commands:
            print('Выход...')	
            clear()
            sys.exit()
        elif command == 'S' or command == 's':
            try:
                for package in range(9, -1, -1):
                    ser.send(db.get(str(package)))
                    with open('log', 'a') as fd:
                        fd.write('{}\n'.format(package))
                    sleep(delay)
                clear()
                break
            except:
#            #except: 'Ошибка последовательного порта'
                print('Не отправлено: {} :(('.format(command))
        else:
            print('Введена неверная команда. Повторите ввод.')
    except KeyboardInterrupt:
        print('Выход...')	
        clear()
        sys.exit()
