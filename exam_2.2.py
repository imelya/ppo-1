#!/usr/bin/env python3
import sys
import subprocess
from config import config

hello_string = '\n===========\nДля вывода последовательнсти цифр числа Pi на МСИ введите S с клавиатуры и нажмите Enter\n\
Для выхода нажмите b и затем Enter\n\n'

args = ["python3", './display.py', 'pi']

stop_commands = config.get('stop_commands')
start_commands = config.get('start_commands')

def clear():
    with open('log', 'w') as fd:
        fd.write('')

while 1:
    try:
        command = input(hello_string)
        if len(command) != 1:
            print('Введена неверная команда. Повторите ввод.')
        elif command in stop_commands:
            print('Выход...')	
            sys.exit()
        elif command in start_commands:
            try:
                s = subprocess.Popen(args)
                command = input('Введетие b для остановки\n')
                if command in stop_commands:
                    s.kill()
                    clear()
            except SerialException:
                print('Не отправлено((')			
        else:
            print('Введена неверная команда. Повторите ввод.')
    except KeyboardInterrupt:
        s.kill()
        print('Выход...')	
        sys.exit()
