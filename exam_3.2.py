#!/usr/bin/env python3
import ser
import voice
import record
from db import db
from datetime import datetime
import os
import sys
import subprocess
from config import config
from time import sleep

hello_string = '==============\nДля вывода последовательности числа e произнесите "СТАРТ"\n\
Для записи нажмите клавишу s и затем Enter\n\
Запись идет в течение 1 секунды\n\
Для выхода нажмите b и затем Enter\n\n'

stop_commands = config.get('stop_commands')
start_commands = config.get('start_commands')

args = ["python3", './display.py', 'e']

def clear():
    with open('log', 'w') as fd:
        fd.write('')

while 1:
    try:
        command = input(hello_string)
        if command == 's' or command == 'S':
            for i in range(1, 0, -1):
                print('Запись начнется через {}'.format(i))
                sleep(1)
            print('Говорите!\n\n')
            record_name = datetime.strftime(datetime.now(), 'record_%y-%m-%d_%H:%M')
            record.record(record_name)
            if os.system('sox records/{0}.wav -r 16000 -b 16 -c 1 records/{0}.raw'.format(record_name)):
                print('Ошибка конвертации звука. Выход...')
                sys.exit(-1)
            output = voice.main('records/{}.raw'.format(record_name))
            if not output:
                print('Повторите\n')
            else:
                transcript = output.get('results')[0].get('alternatives')[0].get('transcript')
                print(transcript)
                if transcript.lower() == 'старт':
                    print('Вы сказали: СТАРТ"')
                    s = subprocess.Popen(args)
                    command = input('\nДля остановки нажмите s, затем Enter и произнесите "СТОП"\n')
                    record_name = datetime.strftime(datetime.now(), 'record_%y-%m-%d_%H:%M')
                    record.record(record_name)
                    if os.system('sox records/{0}.wav -r 16000 -b 16 -c 1 records/{0}.raw'.format(record_name)):
                        print('Ошибка конвертации звука. Выход...')
                        sys.exit(-1)
                    output = voice.main('records/{}.raw'.format(record_name))
                    if not output:
                        print('Повторите\n')
                    else:
                        transcript = output.get('results')[0].get('alternatives')[0].get('transcript')
                        if transcript.lower() == 'стоп':
                            print('Вы сказали: стоп')
                            s.kill()
                            command_in = input('\nДля сброса нажмите s, затем Enter и произнесите "СБРОС"\n')
                            record_name = datetime.strftime(datetime.now(), 'record_%y-%m-%d_%H:%M')
                            record.record(record_name)
                            if os.system('sox records/{0}.wav -r 16000 -b 16 -c 1 records/{0}.raw'.format(record_name)):
                                print('Ошибка конвертации звука. Выход...')
                                sys.exit(-1)
                            output = voice.main('records/{}.raw'.format(record_name))
                            if not output:
                                print('Повторите\n')
                            else:
                                transcript = output.get('results')[0].get('alternatives')[0].get('transcript')
                                print(transcript)
                                if transcript.lower() == 'сброс':
                                    print('Вы сказали: сброс')
                                    clear()
                                    with open('log', 'a') as fd:
                                        fd.write('0')
                                    ser.send(db.get('0'))
                        else:
                            print('Неверная команда, повторите')
                elif command == 'стоп':
                    s.kill()
        elif command in stop_commands:
            s.kill()
            print('Выход...')
            break
    except KeyboardInterrupt:
        s.kill()
        print('Выход...')
        sys.exit()

