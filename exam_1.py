#!/usr/bin/env python3
import ser
from db import db
import sys
from config import config

hello_string = '\n===========\nДля отображения цифры на МСИ введите цифру с клавиатуры и нажмите Enter\n\
Для выхода нажмите b и затем Enter\n\n'

stop_commands = config.get('stop_commands')
start_commands = config.get('start_commands')

def clear():
    with open('log', 'w') as fd:
        fd.write('')

while 1:
    try:
        command = input(hello_string)
        if len(command) != 1:
            print('Введена неверная команда. Повторите ввод.')
        elif command in stop_commands:
            print('Выход...')	
            clear()
            sys.exit()
        elif not command.isdigit():
            print('Введена неверная команда. Повторите ввод.')
        else:
            try:
                with open('log', 'a') as fd:
                        fd.write('{}\n'.format(command))
                ser.send(db.get(command))
                print('Отправлено: {}'.format(command))
            except:
            #except: 'Ошибка последовательного порта'
                print('Не отправлено: {} :(('.format(command))
    except KeyboardInterrupt:
        print('Выход...')	
        clear()
        sys.exit()
