#define A_led 24
#define B_led 26
#define C_led 28
#define D_led 30
#define E_led 32
#define F_led 34
#define G_led 36

void init_led(void){
  pinMode(A_led, OUTPUT);
  pinMode(B_led, OUTPUT);
  pinMode(C_led, OUTPUT);
  pinMode(D_led, OUTPUT);
  pinMode(E_led, OUTPUT);
  pinMode(F_led, OUTPUT);
  pinMode(G_led, OUTPUT);
}

void zero_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, HIGH);
  digitalWrite(F_led, HIGH);
  digitalWrite(G_led, LOW);
}

void one_led(void){
  digitalWrite(A_led, LOW);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, LOW);
  digitalWrite(E_led, LOW);
  digitalWrite(F_led, LOW);
  digitalWrite(G_led, LOW);  
}

void two_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, LOW);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, HIGH);
  digitalWrite(F_led, LOW);
  digitalWrite(G_led, HIGH);  
}

void three_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, LOW);
  digitalWrite(F_led, LOW);
  digitalWrite(G_led, HIGH);  
}

void four_led(void){
  digitalWrite(A_led, LOW);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, LOW);
  digitalWrite(E_led, LOW);
  digitalWrite(F_led, HIGH);
  digitalWrite(G_led, HIGH);  
}

void five_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, LOW);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, LOW);
  digitalWrite(F_led, HIGH);
  digitalWrite(G_led, HIGH);  
}

void six_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, LOW);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, HIGH);
  digitalWrite(F_led, HIGH);
  digitalWrite(G_led, HIGH);  
}

void seven_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, LOW);
  digitalWrite(E_led, LOW);
  digitalWrite(F_led, LOW);
  digitalWrite(G_led, LOW); 
}

void eight_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, HIGH);
  digitalWrite(F_led, HIGH);
  digitalWrite(G_led, HIGH);  
}

void nine_led(void){
  digitalWrite(A_led, HIGH);
  digitalWrite(B_led, HIGH);
  digitalWrite(C_led, HIGH);
  digitalWrite(D_led, HIGH);
  digitalWrite(E_led, LOW);
  digitalWrite(F_led, HIGH);
  digitalWrite(G_led, HIGH);  
}
