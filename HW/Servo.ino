#include <Servo.h>

Servo servo_A;
Servo servo_B;
Servo servo_C;
Servo servo_D;
Servo servo_E;
Servo servo_F;
Servo servo_G;

uint8_t UP = 0;
uint8_t DOWN = 60;

uint8_t pin_A = 2;
uint8_t pin_B = 3;
uint8_t pin_C = 4;
uint8_t pin_D = 5;
uint8_t pin_E = 6;
uint8_t pin_F = 7;
uint8_t pin_G = 8;

uint32_t ser_num_wait = 300;
uint32_t num_wait = 1000;

void init_servo(void){
  
}

void Print(uint8_t A = DOWN, uint8_t B = DOWN, uint8_t C = DOWN, uint8_t D = DOWN, uint8_t E = DOWN, uint8_t F = DOWN, uint8_t G = DOWN)
{
  servo_A.attach(2);
  if(servo_A.read() != A)
  {
   servo_A.write(A);
   delay(ser_num_wait);
  }
  servo_A.detach();
  
  servo_B.attach(3);
  if(servo_B.read() != B)
  {
    servo_B.write(B);
    delay(ser_num_wait);
  }
  servo_B.detach();

  servo_C.attach(4);
  if(servo_C.read() != C)
  {
    servo_C.write(C);
    delay(ser_num_wait);
  }
  servo_C.detach();

  servo_D.attach(5);
  if(servo_D.read() != D)
  {
    servo_D.write(D);
    delay(ser_num_wait);
  }
  servo_D.detach();

  servo_E.attach(6);
  if(servo_E.read() != E)
  {
    servo_E.write(E);
    delay(ser_num_wait);
  }
  servo_E.detach();

  servo_F.attach(7);
  if(servo_F.read() != F)
  {
    servo_F.write(F);
    delay(ser_num_wait);
  }
  servo_F.detach();

  servo_G.attach(8);
  if(servo_G.read() != G)
  {
    servo_G.write(G);
    delay(ser_num_wait);
  }
  servo_G.detach();
}

void zero(){
  Print(UP, UP, UP, UP, UP, UP, DOWN);
  Serial.println("zero");
}

void one(){
  Print(DOWN, UP, UP, DOWN, DOWN, DOWN, DOWN);
  Serial.println("one");
}

void two(){
  Print(UP, UP, DOWN, UP, UP, DOWN, UP);
  Serial.println("two");
}

void three(){
  Print(UP, UP, UP, UP, DOWN, DOWN, UP);
  Serial.println("tree");
}

void four(){
  Print(DOWN, UP, UP, DOWN, DOWN, UP, UP);
  Serial.println("four");
}

void five(){
  Print(UP, DOWN, UP, UP, DOWN, UP, UP);
  Serial.println("five");
}

void six(){
  Print(UP, DOWN, UP, UP, UP, UP, UP);
  Serial.println("six");
}

void seven(){
  Print(UP, UP, UP, DOWN, DOWN, DOWN, DOWN);
  Serial.println("seven");
}

void eight(){
  Print(UP, UP, UP, UP, UP, UP, UP);
  Serial.println("eight");
}

void nine(){
  Print(UP, UP, UP, UP, DOWN, UP, UP);
  Serial.println("nine");
}
