void setup() {
  Serial.begin(9600);
  init_led();
}

void loop() {
  if(Serial.available()) {
    switch(Serial.parseInt()){
      case 0: 
        zero_led();
		    zero();
        break;
      case 1:
        one_led();
        one();
        break;
      case 2:
        two_led();
        two();
        break;
      case 3:
        three_led();
        three();
        break;
      case 4:
        four_led();
        four();
        break;
      case 5:
        five_led();
        five();
        break;
      case 6:
        six_led();
        six();
        break;
      case 7:
        seven_led();
        seven();
        break;
      case 8:
        eight_led();
        eight();
        break;
      case 9:
        nine_led();
        nine();
        break;
      default:
        zero_led();
        zero();
        break;
    }
  }
}
