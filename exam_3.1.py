#!/usr/bin/env python3
import voice
import record
import os
import time
import sys
from datetime import datetime
import ser
from db import db
from config import config

hello_string = '==============\nДля записи нажмите клавишу s и затем Enter\n\
Запись идет в течение 1 секунды\n\
Для выхода нажмите b и затем Enter\n\n'

stop_commands = config.get('stop_commands')
start_commands = config.get('start_commands')

def clear():
    with open('log', 'w') as fd:
        fd.write('')

while 1:
    try:
        command = input(hello_string)
        if command in start_commands:
            for i in range(1, 0, -1):
                print('Запись начнется через {}'.format(i))
                time.sleep(1)
            print('Говорите!\n\n')
            record_name = datetime.strftime(datetime.now(), 'record_%y-%m-%d_%H:%M')
            record.record(record_name)
            if os.system('sox records/{0}.wav -r 16000 -b 16 -c 1 records/{0}.raw'.format(record_name)):
                print('Ошибка конвертации звука. Выход...')
                sys.exit(-1)
            output = voice.main('records/{}.raw'.format(record_name))
            if not output:
                print('Повторите\n')
            else:
                transcript = output.get('results')[0].get('alternatives')[0].get('transcript')
                if db.get(transcript):
                    print('Вы сказали: "{}"'.format(transcript))
                    ser.send(db.get(transcript))
                    with open('log', 'a') as fd:
                        fd.write('{}\n'.format(transcript))
                else:
                    print(output.get('results')[0].get('alternatives')[0].get('transcript'))
#                    print('Неверная команда, повторите')

        elif command in stop_commands:
            print('Выход...')
            clear()
            break
        else:
            print('Введите s для начала записи\n')
    except KeyboardInterrupt:
        print('Выход...')
        clear()
        sys.exit()
