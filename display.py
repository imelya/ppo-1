from math import e, pi
import ser
from db import db
from time import sleep
import sys
from config import config
from decimal import getcontext 
from get_pi import chudnovsky

delay = config.get('delay')

def display(commmand):
    try:
        if commmand == 'e':
            sequence = str(e)[2:]
        elif commmand == 'pi':
            getcontext().prec = 100
            sequence = str(chudnovsky(1000))[2:]
        else:
            sys.exit()
        for num in sequence:
            sleep(delay)
            ser.send(num.encode())
            with open('log', 'a') as fd:
                fd.write('{}\n'.format(num))
    except KeyboardInterrupt:
        sys.exit()

if sys.argv[1] == 'pi':
    display('pi')
elif sys.argv[1] == 'e':
    display('e')
