config = {"delay": 3,
        "start_commands": ["S", "s"],
        "stop_commands": ["B", "b", "q"],
        "RECORD_SECONDS": 1.2,
        "ACM0": '/dev/ttyACM0',
        "ACM1": '/dev/ttyACM1'
        }
